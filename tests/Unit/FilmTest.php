<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use \App\Classes\Film;
class FilmTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }


    protected function setUp(): void
    {
        $this->film = new Film();
    }
    /**
     * @dataProvider yearDataProvider
     * @param $actual
     * @param $expected
     */

    public function testYear($actual, $expected)
    {
        $result = $this->year = $actual;
        $this->assertEquals($expected, $result);
    }

    public function yearDataProvider()
    {
        return array(
            array(1999, 1999),
            array(235, 235),
            array(2010, 2010)
        );
    }

    protected function tearDown(): void
    {
    }


    public function testStub()
    {
        $stub = $this->getMockBuilder(\App\Classes\Film::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $stub->method('getName')
            ->willReturn(('Crash'));

        $this->assertSame('Crash', $stub->getName());
    }

}
