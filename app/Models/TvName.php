<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TVname extends Model
{
    use HasFactory;

    protected $table = "tvName";
    public $timestamps = false;

    public function query_name()
    {
        return DB::table('tvName', 't')
            ->join('genre_list as gl', 'gl.genre_id', '=', 't.genre_id')
            ->select('t.*', 'gl.*');
    }
    public function data()
    {
        return $this->query_name()
            ->orderBy('t.time')
            ->get();
    }
    public function oneData($name)
    {
        return $this->query_name()
            ->where('t.name', 'like', "$name%")
            ->get();
    }

    public function oneDataById($id)
    {
        return $this->query_name()
            ->where('t.id', '=', "$id")
            ->get();
    }
    public function sort($name)
    {
        return $this->query_name()
            ->orderBy('t.time')
            ->where('gl.genre_name', $name)
            ->get();
    }

}
