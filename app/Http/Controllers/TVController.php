<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TVController extends Controller
{
    public function index()
    {
        return view('guest.home', ['data' => (new \App\Models\TVname)->data(),'genres' => (new \App\Models\Genre_list())->genres()]);
    }
    public function sort($name)
    {
        return view('guest.home', ['data' => (new \App\Models\TVname)->sort($name), 'genres' => (new \App\Models\Genre_list())->genres()]);
    }

    public function find(Request $req)
    {

        $name = $req->name;

        return view('guest.find', ['data' => (new \App\Models\TVname)->oneData($name)]);
    }
}
