<?php

namespace App\Http\Controllers;

use App\Models\TVname;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/index', ['data' => (new \App\Models\TVname)->data()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/add', ['genres' => (new \App\Models\genre_list())->genres()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new TVname;
        $add->name = $request->input('name');
        $add->time = $request->input('time');
        $add->genre_id = $request->input('genre');
        $add->age_rate = $request->input('age_rate');
        $add->save();
        return redirect()->route('program.index')->with('success', 'Програма додана');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.show', ['data' => (new \App\Models\TVname)->oneDataById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', [
            'result' => (new \App\Models\TVname)->oneDataById($id),
            'genres' => (new \App\Models\genre_list())->genres()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('TvName')
            ->where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'time' => $request->input('time'),
                'genre_id' => $request->input('genre'),
                'age_rate' => $request->input('age_rate')
            ]);
        return redirect()->route('program.index')->with('success', 'Програма відредагована');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('TVname')->where('id', '=', $id)->delete();
        return redirect()->route('program.index')->with('success', 'Програма видалена');
    }
}
