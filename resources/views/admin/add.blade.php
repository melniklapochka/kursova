@extends('admin.layouts.schema')

@section('title',"Admin add")

@section('content')
    <form action="{{route('program.store')}}" method="post">
        @csrf
        <p>
            <label for="name">Назва програми:</label>
            <input type="text" name="name" id="name" required>
        </p>
        <p>
            <label for="time">Час програми:</label>
            <input type="time" name="time" id="time" required>
        </p>
        <p>
            <label for="genre">Жанр:</label>
            <select id="genre" name="genre">
                @foreach($genres as $genre)
                    <option value='{{ $genre->genre_id }}'>{{ $genre->genre_name }}</option>
                @endforeach
            </select>
        </p>
        <p>
            <label for="age">Вікове обмеження:</label>
            <input type="text" id="age" name="age_rate" required>
        </p>
        <button type="submit" class="btn btn-primary">Додати</button>
        <a href="{{route('program.index')}}" class="btn btn-secondary">Назад</a>
    </form>
@endsection

