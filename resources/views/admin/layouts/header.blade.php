<header class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <p class="h5 my-0 me-md-auto fw-normal">Lapochka production</p>
    <nav class="my-2 my-md-0 me-md-3">
        <a class="p-2 text-dark" href="{{route('program.index')}}">На головну</a>
        <a class="p-2 text-dark" href="{{route('program.create')}}">Додати програму</a>
    </nav>
    <a class="btn btn-outline-primary" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</header>


