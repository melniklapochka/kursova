@extends('admin.layouts.schema')
@foreach($result as $row)

@section('title',$row->name . ' edit')

@section('content')
    <form action="{{route('program.update',['id' => $row->id])}}" method="post">
        <input name="_method" type="hidden" value="PUT">
        @csrf
        <p>
            <label for="name">Назва програми:</label>
            <input type="text" name="name" id="name" value="{{$row->name}}" required>
        </p>
        <p>
            <label for="time">Час програми:</label>
            <input type="time" name="time" id="time" value="{{$row->time}}" required>
        </p>
        <p>
            <label for="genre">Жанр:</label>
            <select id="genre" name="genre">
                @foreach($genres as $genre)
                    <option value='{{ $genre->genre_id }}'>{{ $genre->genre_name }}</option>
                @endforeach
            </select>
        </p>
        <p>
            <label for="age">Вікове обмеження:</label>
            <input type="text" id="age" name="age_rate" value="{{$row->age_rate}}" required>
        </p>
        <button type="submit" class="btn btn-primary">Редагувати</button>
        <a href="{{route('program.index')}}" class="btn btn-secondary">Назад</a>
    </form>
@endsection
@endforeach
