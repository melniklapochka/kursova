@extends('guest.layouts.schema')

@section('title', "Find")

@section('content')
    <div class="d-flex justify-content-center">
    <form action="{{route('find-form')}}" method="post">
        @csrf
        <p>
        <label for="name">Назва програми:</label>
        <input type="text" placeholder="Назва програми" name="name" id="name">
            <p>
        <button type="submit" class="btn btn-primary">Пошук</button>
        <a href="{{route('index')}}" class="btn btn-secondary">Назад</a>
        </p>
    </form>
    </div>
@endsection()
