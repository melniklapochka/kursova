<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>@yield('title')</title>
</head>
<body>
@include('guest.layouts.header')
@yield('content')
@if(isset($genres))
    @foreach($genres as $genre)
        <a href="{{route('sort', ['name'=>$genre->genre_name])}}">{{$genre->genre_name}}</a>
    @endforeach
@endif
@if(isset($data))
<div class="container">
    <table class="table table-danger table-striped">
        <thead>
        <th>Назва программи</th>
        <th>Час</th>
        <th>Жанр</th>
        <th>Вікове обмеження</th>
        </thead>
        <tbody>
        @foreach ($data as $row)
        <tr>
            <td width="300" height="60">
                {{$row->name}}
            </td>
            <td width="300">
                {{$row->time}}
            </td>
            <td width="300">
                {{$row->genre_name}}
            </td>
            <td width="300">
                {{$row->age_rate}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
    @endif
</body>
</html>
