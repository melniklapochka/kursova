<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TVController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TVController::class, 'index'])->name('index');
Route::get('/genre/{name}', [TVController::class, 'sort'])->name('sort');

Route::get('/find', function () {
    return view('guest.find');
})->name('find');

Route::post('/find/submit', [TVController::class, 'find'])->name('find-form');

Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
    'register' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
    Route::resource('admin/program', 'App\Http\Controllers\AdminController', ['parameters' => [
        'program' => 'id'
    ]]);
});
